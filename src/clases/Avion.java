/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Ray
 */
public class Avion {

    /**
     * @return the idAvion
     */
    public int getIdAvion() {
        return idAvion;
    }

    /**
     * @param idAvion the idAvion to set
     */
    public void setIdAvion(int idAvion) {
        this.idAvion = idAvion;
    }

    /**
     * @return the capacidadAvion
     */
    public int getCapacidadAvion() {
        return capacidadAvion;
    }

    /**
     * @param capacidadAvion the capacidadAvion to set
     */
    public void setCapacidadAvion(int capacidadAvion) {
        this.capacidadAvion = capacidadAvion;
    }

    /**
     * @return the modeloAvion
     */
    public String getModeloAvion() {
        return modeloAvion;
    }

    /**
     * @param modeloAvion the modeloAvion to set
     */
    public void setModeloAvion(String modeloAvion) {
        this.modeloAvion = modeloAvion;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    private int idAvion;
    private int capacidadAvion;
    private String modeloAvion;
    private String tipo;
    
}
