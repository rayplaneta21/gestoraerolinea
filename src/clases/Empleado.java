/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import comun.cDatos;
import java.sql.ResultSet;

/**
 *
 * @author Ray
 */
public class Empleado {

    /**
     * @return the contra
     */
    public String getContra() {
        return contra;
    }

    /**
     * @param contra the contra to set
     */
    public void setContra(String contra) {
        this.contra = contra;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the aPat
     */
    public String getaPat() {
        return aPat;
    }

    /**
     * @param aPat the aPat to set
     */
    public void setaPat(String aPat) {
        this.aPat = aPat;
    }

    /**
     * @return the aMat
     */
    public String getaMat() {
        return aMat;
    }

    /**
     * @param aMat the aMat to set
     */
    public void setaMat(String aMat) {
        this.aMat = aMat;
    }

    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the edad
     */
    public String getFechaNac() {
        return fechaNac;
    }

    /**
     * @param edad the edad to set
     */
    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the puesto
     */
    public String getPuesto() {
        return puesto;
    }

    /**
     * @param puesto the puesto to set
     */
    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }
    
    private String usuario;
    private String nombre;
    private String aPat;
    private String aMat;
    private String sexo;
    private String fechaNac;
    private String direccion;
    private String tipo;
    private String puesto;
    private String contra;
    

}
