/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Ray
 */
public class Cliente {

    /**
     * @return the idCliente
     */
    public int getIdCliente() {
        return idCliente;
    }

    /**
     * @param idCliente the idCliente to set
     */
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the aPat
     */
    public String getaPat() {
        return aPat;
    }

    /**
     * @param aPat the aPat to set
     */
    public void setaPat(String aPat) {
        this.aPat = aPat;
    }

    /**
     * @return the aMat
     */
    public String getaMat() {
        return aMat;
    }

    /**
     * @param aMat the aMat to set
     */
    public void setaMat(String aMat) {
        this.aMat = aMat;
    }

    /**
     * @return the correo
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * @param correo the correo to set
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    private int idCliente;
    private String nombre;
    private String aPat;
    private String aMat;
    private String correo;
    
}
