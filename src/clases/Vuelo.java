/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Ray
 */
public class Vuelo {

    /**
     * @return the idVuelo
     */
    public int getIdVuelo() {
        return idVuelo;
    }

    /**
     * @param idVuelo the idVuelo to set
     */
    public void setIdVuelo(int idVuelo) {
        this.idVuelo = idVuelo;
    }

    /**
     * @return the idAvion
     */
    public int getIdAvion() {
        return idAvion;
    }

    /**
     * @param idAvion the idAvion to set
     */
    public void setIdAvion(int idAvion) {
        this.idAvion = idAvion;
    }

    /**
     * @return the idSalida
     */
    public int getIdSalida() {
        return idSalida;
    }

    /**
     * @param idSalida the idSalida to set
     */
    public void setIdSalida(int idSalida) {
        this.idSalida = idSalida;
    }

    /**
     * @return the idDestino
     */
    public int getIdDestino() {
        return idDestino;
    }

    /**
     * @param idDestino the idDestino to set
     */
    public void setIdDestino(int idDestino) {
        this.idDestino = idDestino;
    }

    /**
     * @return the fechaSalida
     */
    public String getFechaSalida() {
        return fechaSalida;
    }

    /**
     * @param fechaSalida the fechaSalida to set
     */
    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    /**
     * @return the fechaLlegada
     */
    public String getFechaLlegada() {
        return fechaLlegada;
    }

    /**
     * @param fechaLlegada the fechaLlegada to set
     */
    public void setFechaLlegada(String fechaLlegada) {
        this.fechaLlegada = fechaLlegada;
    }

    /**
     * @return the nombreSalida
     */
    public String getNombreSalida() {
        return nombreSalida;
    }

    /**
     * @param nombreSalida the nombreSalida to set
     */
    public void setNombreSalida(String nombreSalida) {
        this.nombreSalida = nombreSalida;
    }

    /**
     * @return the nombreLlegada
     */
    public String getNombreLlegada() {
        return nombreLlegada;
    }

    /**
     * @param nombreLlegada the nombreLlegada to set
     */
    public void setNombreLlegada(String nombreLlegada) {
        this.nombreLlegada = nombreLlegada;
    }

    /**
     * @return the lugares
     */
    public int getLugares() {
        return lugares;
    }

    /**
     * @param lugares the lugares to set
     */
    public void setLugares(int lugares) {
        this.lugares = lugares;
    }

    private int idVuelo;
    private int idAvion;
    private int idSalida;
    private int idDestino;
    private String fechaSalida;
    private String fechaLlegada;
    private String nombreSalida;
    private String nombreLlegada;
    private int lugares;
}
