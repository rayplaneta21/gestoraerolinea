/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Ray
 */
public class Destino {

    /**
     * @return the idDestino
     */
    public int getIdDestino() {
        return idDestino;
    }

    /**
     * @param idDestino the idDestino to set
     */
    public void setIdDestino(int idDestino) {
        this.idDestino = idDestino;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the pais
     */
    public String getPais() {
        return pais;
    }

    /**
     * @param pais the pais to set
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * @return the aeropuerto
     */
    public String getAeropuerto() {
        return aeropuerto;
    }

    /**
     * @param aeropuerto the aeropuerto to set
     */
    public void setAeropuerto(String aeropuerto) {
        this.aeropuerto = aeropuerto;
    }
    
    private int idDestino;
    private String ciudad;
    private String pais;
    private String aeropuerto;
}
