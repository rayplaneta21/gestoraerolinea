/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import comun.cDatos;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ray
 */
public class Administrador {
    
     public Empleado validaEmpleado(String usuario, String contra)
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        Empleado emp = new Empleado();
        try
        {
            sql.conectar();
            System.out.println("Conecta");
            r = sql.consulta("select usuario,nombre, aPat, aMat, sexo, fechaNacimiento, direccion, tipo, puesto from empleado where usuario = '"+usuario+"' and password = '"+contra+"'");
            if(r.next())
            {
                emp.setUsuario(r.getString(1));
                emp.setNombre(r.getString(2)); 
                emp.setaPat(r.getString(3));
                emp.setaMat(r.getString(4));
                emp.setSexo(r.getString(5));
                emp.setFechaNac(r.getString(6));
                emp.setDireccion(r.getString(7));
                emp.setTipo(r.getString(8));
                emp.setPuesto(r.getString(9));
                
            }
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return emp;
    }
    
    public Empleado obtieneEmpleado(String usuario)
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        Empleado emp = new Empleado();
        try
        {
            sql.conectar();
            System.out.println("Conecta");
            r = sql.consulta("select * from empleado where usuario = '"+usuario+"'");
            if(r.next())
            {
                emp.setUsuario(r.getString(1));
                emp.setNombre(r.getString(2)); 
                emp.setaPat(r.getString(3));
                emp.setaMat(r.getString(4));
                emp.setSexo(r.getString(5));
                emp.setFechaNac(r.getString(6));
                emp.setDireccion(r.getString(7));
                emp.setTipo(r.getString(8));
                emp.setPuesto(r.getString(9));
                emp.setContra(r.getString(10));
            }
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return emp;
    }
    
    public ArrayList<Empleado> listarEmpleados()
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        Empleado emp = null;
        ArrayList<Empleado> lista = new ArrayList<Empleado> ();
        try
        {
            sql.conectar();
            System.out.println("Conecta");
            r = sql.consulta("select usuario,nombre, aPat, aMat, sexo, fechaNacimiento, direccion, tipo, puesto from empleado ");
            while(r.next())
            {
                emp = new Empleado();
                emp.setUsuario(r.getString(1));
                emp.setNombre(r.getString(2)); 
                emp.setaPat(r.getString(3));
                emp.setaMat(r.getString(4));
                emp.setSexo(r.getString(5));
                emp.setFechaNac(r.getString(6));
                emp.setDireccion(r.getString(7));
                emp.setTipo(r.getString(8));
                emp.setPuesto(r.getString(9));
                lista.add(emp);
                
            }
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return lista;
    }
    
    public boolean altaEmp(Empleado emp)
    {
        boolean correcto = false;
        cDatos sql = new cDatos();
        
        try
        {
            sql.conectar();
            sql.insertUpd("insert into empleado values('"+emp.getUsuario()+"'"
                    + ",'"+emp.getNombre()+"'"
                    + ",'"+emp.getaPat()+"'"
                    + ",'"+emp.getaMat()+"'"
                    + ",'"+emp.getSexo()+"'"
                    + ",'"+emp.getFechaNac()+"'"
                    + ",'"+emp.getDireccion()+"'"
                    + ",'"+emp.getTipo()+"'"
                    + ",'"+emp.getPuesto()+"'"
                    + ",'"+emp.getContra()+"')");
            correcto = true;
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            correcto = false;
        }
        return correcto;
    }
    
    public boolean borrarEmpleado(String usuario)
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        Empleado emp = null;
        boolean borra = false;
        try
        {
            sql.conectar();
            System.out.println("Conecta");
            sql.insertUpd("delete from tripulacion where usuarioEmp = '"+usuario+"' ");
            sql.insertUpd("delete from empleado where usuario = '"+usuario+"' ");
            borra = true;
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return borra;
    }
    
    public boolean modEmpleado(Empleado emp)
    {
       boolean correcto = false;
       cDatos sql = new cDatos();
       ResultSet r = null;
       try
        {
            sql.conectar();
            System.out.println("Conecta");
            
            sql.insertUpd("update empleado set "
                    + "nombre = '"+emp.getNombre()+"', "
                    + "aPat = '"+emp.getaPat()+"', "
                    + "aMat = '"+emp.getaMat()+"', "
                    + "sexo = '"+emp.getSexo()+"', "
                    + "fechaNacimiento = '"+emp.getFechaNac()+"', "
                    + "direccion = '"+emp.getDireccion()+"', "
                    + "tipo = '"+emp.getTipo()+"', "
                    + "puesto = '"+emp.getPuesto()+"' "
                    + "where usuario = '"+emp.getUsuario()+"'");
            
            
            sql.cierraConexion();
            correcto = true;
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }       
       return correcto;
    }
    
    public boolean altaCliente(Cliente cliente)
    {
        boolean correcto = false;
        cDatos sql = new cDatos();
        ResultSet r = null;
        int cons = 0;
        try
        {
            sql.conectar();
            cons = sql.consecutivo("cliente");
            sql.insertUpd("insert into clientes values("+cons+""
                    + ",'"+cliente.getNombre()+"'"
                    + ",'"+cliente.getaPat()+"'"
                    + ",'"+cliente.getaMat()+"'"
                    + ",'"+cliente.getCorreo()+"')");
            correcto = true;
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            correcto = false;
        }
        return correcto;
    }
    
    public Cliente obtieneCliente(int idCliente)
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        Cliente cliente = new Cliente();
        try
        {
            sql.conectar();
            r = sql.consulta("select * from clientes where idCliente = "+idCliente+"");
            if(r.next())
            {
                cliente.setIdCliente(r.getInt(1));
                cliente.setNombre(r.getString(2)); 
                cliente.setaPat(r.getString(3));
                cliente.setaMat(r.getString(4));
                cliente.setCorreo(r.getString(5));              
            }
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return cliente;
    }
    
    public ArrayList<Cliente> listarClientes()
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        Cliente cliente = null;
        ArrayList<Cliente> lista = new ArrayList<Cliente> ();
        try
        {
            sql.conectar();
            r = sql.consulta("select * from clientes");
            while(r.next())
            {
                cliente = new Cliente();
                cliente.setIdCliente(r.getInt(1));
                cliente.setNombre(r.getString(2)); 
                cliente.setaPat(r.getString(3));
                cliente.setaMat(r.getString(4));
                cliente.setCorreo(r.getString(5));   
                lista.add(cliente);
            }
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return lista;
    }
    
    public boolean borrarCliente(int idCliente)
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        boolean borra = false;
        try
        {
            sql.conectar();
            System.out.println("Conecta");
            sql.insertUpd("delete from pasajeros where idCliente = "+idCliente+" ");
            sql.insertUpd("delete from clientes where idCliete = "+idCliente+" ");
            borra = true;
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return borra;
    }
    
     public boolean modCliente(Cliente cliente)
    {
       boolean correcto = false;
       cDatos sql = new cDatos();
       try
        {
            sql.conectar();            
            sql.insertUpd("update clientes set "
                    + "nombre = '"+cliente.getNombre()+"', "
                    + "aPat = '"+cliente.getaPat()+"', "
                    + "aMat = '"+cliente.getaMat()+"', "
                    + "correo = '"+cliente.getCorreo()+"' "
                    + "where idCliente = "+cliente.getIdCliente()+"");
            
            
            sql.cierraConexion();
            correcto = true;
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }       
       return correcto;
    }
     
    public boolean altaAvion(Avion avion)
    {
        boolean correcto = false;
        cDatos sql = new cDatos();
        ResultSet r = null;
        int cons = 0;
        try
        {
            sql.conectar();
            cons = sql.consecutivo("avion");
            sql.insertUpd("insert into avion values("+ cons +""
                    + ",'"+avion.getModeloAvion()+"'"
                    + ","+avion.getCapacidadAvion()+""
                    + ",'"+avion.getTipo()+"')");
            correcto = true;
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            correcto = false;
        }
        return correcto;
    }
    
    public Avion obtieneAvion(int idAvion)
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        Avion avion = new Avion();
        try
        {
            sql.conectar();
            r = sql.consulta("select * from avion where idAvion = "+idAvion+"");
            if(r.next())
            {
                avion.setIdAvion(r.getInt(1));
                avion.setModeloAvion(r.getString(2)); 
                avion.setCapacidadAvion(r.getInt(3));
                avion.setTipo(r.getString(4));             
            }
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return avion;
    }
    
    public ArrayList<Avion> listarAviones()
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        Avion avion = null;
        ArrayList<Avion> lista = new ArrayList<Avion> ();
        try
        {
            sql.conectar();
            r = sql.consulta("select * from avion");
            while(r.next())
            {
                avion = new Avion();
                avion.setIdAvion(r.getInt(1));
                avion.setModeloAvion(r.getString(2)); 
                avion.setCapacidadAvion(r.getInt(3));
                avion.setTipo(r.getString(4));
                lista.add(avion);
            }
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return lista;
    }

    public boolean borrarAvion(int idAvion)
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        boolean borra = false;
        try
        {
            sql.conectar();
            System.out.println("Conecta");
            sql.insertUpd("update vuelo set idAvion = 0 where idAvion = "+idAvion+" ");
            sql.insertUpd("delete from avion where idAvion = "+idAvion+" ");
            borra = true;
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return borra;
    }
    
     public boolean modAvion(Avion avion)
    {
       boolean correcto = false;
       cDatos sql = new cDatos();
       try
        {
            sql.conectar();            
            sql.insertUpd("update avion set "
                    + "modelo = '"+avion.getModeloAvion()+"', "
                    + "capacidadPasajeros = "+avion.getCapacidadAvion()+", "
                    + "tipoAvion = '"+avion.getTipo()+"'"
                    + "where idAvion = "+avion.getIdAvion()+"");
            
            
            sql.cierraConexion();
            correcto = true;
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }       
       return correcto;
    }
     
    public boolean altaDestino(Destino destino)
    {
        boolean correcto = false;
        cDatos sql = new cDatos();
        int cons = 0;
        try
        {
            sql.conectar();
            cons = sql.consecutivo("destino");
            sql.insertUpd("insert into lugar values("+cons+""
                    + ",'"+destino.getCiudad()+"'"
                    + ", '"+destino.getPais()+"' "
                    + ",'"+destino.getAeropuerto()+"')");
            correcto = true;
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            correcto = false;
        }
        return correcto;
    }
    
    public Destino obtieneDestino(int idDestino)
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        Destino destino = new Destino();
        try
        {
            sql.conectar();
            r = sql.consulta("select * from lugar where idLugar = "+idDestino+"");
            if(r.next())
            {
                destino.setIdDestino(r.getInt(1));
                destino.setCiudad(r.getString(2)); 
                destino.setPais(r.getString(3));
                destino.setAeropuerto(r.getString(4));             
            }
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return destino;
    }
    
    public ArrayList<Destino> listarDestinos()
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        Destino destino = null;
        ArrayList<Destino> lista = new ArrayList<Destino> ();
        try
        {
            sql.conectar();
            r = sql.consulta("select * from lugar");
            while(r.next())
            {
                destino = new Destino();
                destino.setIdDestino(r.getInt(1));
                destino.setCiudad(r.getString(2)); 
                destino.setPais(r.getString(3));
                destino.setAeropuerto(r.getString(4));  
                lista.add(destino);
            }
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return lista;
    }

    public boolean borrarDestino(int idDestino)
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        boolean borra = false;
        try
        {
            sql.conectar();
            System.out.println("Conecta");
            sql.insertUpd("update vuelo set idSalida = 0 where idSalida = "+idDestino+" ");
            sql.insertUpd("update vuelo set idDestino = 0 where idDestino = "+idDestino+" ");
            sql.insertUpd("delete from lugar where idLugar = "+idDestino+" ");
            borra = true;
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return borra;
    }
    
     public boolean modDestino(Destino destino)
    {
       boolean correcto = false;
       cDatos sql = new cDatos();
       try
        {
            sql.conectar();            
            sql.insertUpd("update lugar set "
                    + "ciudad = '"+destino.getCiudad()+"', "
                    + "pais = '"+destino.getPais()+"', "
                    + "aeropuerto = '"+destino.getAeropuerto()+"'"
                    + "where idLugar = "+destino.getIdDestino()+"");
            
            
            sql.cierraConexion();
            correcto = true;
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }       
       return correcto;
    }
    
    public boolean altaVuelo(Vuelo vuelo)
    {
        boolean correcto = false;
        cDatos sql = new cDatos();
        ResultSet r = null;
        int cons = 0;
        int cap = 0;
        try
        {
            sql.conectar();
            r = sql.consulta("select capacidadPasajeros from avion where idAvion = "+vuelo.getIdAvion()+"");
            if(r.next())
            {
              cap = r.getInt(1);            
            }
            cons = sql.consecutivo("vuelo");
            sql.insertUpd("insert into vuelo values("+cons+""
                    + ", "+vuelo.getIdAvion()+" "
                    + ", "+vuelo.getIdSalida()+" "
                    + ", "+vuelo.getIdDestino()+" "
                    + ", '"+vuelo.getFechaSalida()+"' "
                    + ", '"+vuelo.getFechaLlegada()+"' "
                    + ", "+cap+")");
            correcto = true;
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            correcto = false;
        }
        return correcto;
    }
    
    public Vuelo obtieneVuelo(int idVuelo)
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        Vuelo vuelo = new Vuelo();
        try
        {
            sql.conectar();
            r = sql.consulta("select * from vuelo where idVuelo = "+idVuelo+"");
            if(r.next())
            {
                vuelo.setIdVuelo(r.getInt(1));       
                vuelo.setIdAvion(r.getInt(2)); 
                vuelo.setIdSalida(r.getInt(3)); 
                vuelo.setIdDestino(r.getInt(4));
                vuelo.setFechaSalida(r.getString(5)); 
                vuelo.setFechaLlegada(r.getString(6)); 
                vuelo.setLugares(r.getInt(7));
            }
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return vuelo;
    }
    
    public ArrayList<Vuelo> listarVuelos()
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        Vuelo vuelo = null;
        ArrayList<Vuelo> lista = new ArrayList<Vuelo> ();
        try
        {
            sql.conectar();
            r = sql.consulta("select * from vuelo");
            while(r.next())
            {
                vuelo = new Vuelo();
                vuelo.setIdVuelo(r.getInt(1));       
                vuelo.setIdAvion(r.getInt(2)); 
                vuelo.setIdSalida(r.getInt(3)); 
                vuelo.setIdDestino(r.getInt(4));
                vuelo.setFechaSalida(r.getString(5)); 
                vuelo.setFechaLlegada(r.getString(6)); 
                vuelo.setLugares(r.getInt(7));  
                lista.add(vuelo);
            }
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return lista;
    }

    public boolean borrarVuelo(int idVuelo)
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        boolean borra = false;
        try
        {
            sql.conectar();
            System.out.println("Conecta");
            sql.insertUpd("delete from pasajeros  where idVuelo = "+idVuelo+" ");
            sql.insertUpd("delete from tripulacion  where idVuelo = "+idVuelo+" ");
            sql.insertUpd("delete from vuelo where idVuelo = "+idVuelo+" ");
            borra = true;
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return borra;
    }
    
    public boolean modVuelo(Vuelo vuelo)
    {
       boolean correcto = false;
       cDatos sql = new cDatos();
       try
        {
            sql.conectar();            
            sql.insertUpd("update vuelo set "
                    + "idAvion = "+vuelo.getIdAvion()+", "
                    + "idSalida = "+vuelo.getIdSalida()+", "
                    + "idDestino = "+vuelo.getIdDestino()+", "
                    + "fechaSalida = '"+vuelo.getFechaSalida()+"', "
                    + "fechaLlegada = '"+vuelo.getFechaLlegada()+"' "       
                    + "where idVuelo = "+vuelo.getIdVuelo()+"");            
            sql.cierraConexion();
            correcto = true;
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }       
       return correcto;
    }    
    
    public String formatoFecha(String fechaEnt)
    {
        String fechaSalida = "";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date fecha = new Date(fechaEnt);
        fechaSalida = formatter.format(fecha);
        System.out.println(fechaSalida);
        return fechaSalida;
    }
    
    public Date formatoFecha1(String fechaEnt)
    {
        String fechaSalida = "";
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm aa");
        Date fecha = new Date(fechaEnt);
        fechaSalida = formatter.format(fecha);
        System.out.println(fechaSalida);
        Date res = new Date(fechaSalida);
        return res;
    }
    
    public ArrayList<Vuelo> listarAsigna()
    {         
        cDatos sql = new cDatos();
        ResultSet r = null;
        Vuelo vuelo = null;
        ArrayList<Vuelo> lista = new ArrayList<Vuelo> ();
        try
        {
            sql.conectar();
            r = sql.consulta("select u.idVuelo,(select CONCAT(l.ciudad, ', ', l.pais, ', ', l.aeropuerto) from vuelo v, lugar l where l.idLugar = v.idSalida and idVuelo = u.idVuelo) as salida, (select CONCAT(l.ciudad, ', ', l.pais, ', ', l.aeropuerto) from vuelo v, lugar l where l.idLugar = v.idDestino and idVuelo = u.idVuelo) as destino, u.fechaSalida, u.fechaLlegada, u.lugDisponibles from vuelo u where u.lugDisponibles > 0 and fechaSalida > now()");
            while(r.next())
            {
                vuelo = new Vuelo();
                vuelo.setIdVuelo(r.getInt(1));   
                vuelo.setNombreSalida(r.getString(2)); 
                vuelo.setNombreLlegada(r.getString(3));
                vuelo.setFechaSalida(r.getString(5)); 
                vuelo.setFechaLlegada(r.getString(6)); 
                vuelo.setLugares(r.getInt(7));  
                lista.add(vuelo);
            }
            sql.cierraConexion();
        }
        catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        return lista;
    }
    
    public boolean asignaVuelo(String cliente, int vuelo, int tipo)
    {
        boolean correcto = false;
        cDatos sql = new cDatos();
        int cap = 0;
        int lug = 0;
        ResultSet r = null;
        try
        {
            if(tipo == 1)
            {
                sql.conectar();
                r = sql.consulta("select lugDisponibles from vuelo where idVuelo= "+vuelo+"");
                if(r.next())
                {
                  cap = r.getInt(1);            
                }
                lug = cap - 1;
                sql.insertUpd("insert into pasajeros values("+cliente+", "+vuelo+")");
                sql.insertUpd("update pasajeros set lugDisponibles = "+lug+" where idVuelo = "+vuelo+")");
                correcto = true;
                sql.cierraConexion();
            }
            else
            {
                if(tipo == 2)
                {
                    sql.conectar();     
                    sql.insertUpd("insert into tripulacion values('"+cliente+"', "+vuelo+")");
                    correcto = true;
                    sql.cierraConexion();
                }
            }
            
        }
        catch(Exception e)
        {
            correcto = false;
        }
        return correcto;
    }
}
