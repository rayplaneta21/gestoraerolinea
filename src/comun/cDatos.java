/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comun;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Ray
 */
public class cDatos {
    
    private String usrBD;
    private String passBD;
    private String urlBD;
    private String driverClassName;
    private Connection conn = null;
    private Statement estancia;
 
    public cDatos(String usuarioBD, String passwordBD, String url, String driverClassName) {
        this.usrBD = usuarioBD;
        this.passBD = passwordBD;
        this.urlBD = url;
        this.driverClassName = driverClassName;
    }
    public cDatos() {
        //poner los datos apropiados
        this.usrBD = "root";
        this.passBD = "root";
        this.urlBD = "jdbc:mysql://localhost:3306/aerolinea";
        this.driverClassName = "com.mysql.jdbc.Driver";
    }
   
    //Conexion a la BD
    public void conectar() throws SQLException {
        try {
            Class.forName(this.driverClassName).newInstance();
            this.conn = DriverManager.getConnection(this.urlBD, this.usrBD, this.passBD);
 
        } catch (Exception err) {
            System.out.println("Error " + err.getMessage());
        }
    }    
    //Cerrar la conexion de BD
    public void cierraConexion() throws SQLException {
        this.conn.close();
    }
    
    //Metodos para ejecutar sentencias SQL
    public ResultSet consulta(String consulta) throws SQLException {
        this.estancia = (Statement) conn.createStatement();
        System.out.println(consulta);
        return this.estancia.executeQuery(consulta);
    }   
    
    public void insertUpd(String consulta) throws SQLException {
        this.estancia = (Statement) conn.createStatement();
        System.out.println(consulta);
        this.estancia.execute(consulta);
    } 
    
    public int consecutivo(String tipo)
    {
        int cons = 0;
        int aux = 0;
        ResultSet r = null;
        try
        {
            this.conectar();
            r = this.consulta("select cons from consecutivo where tipo = '"+tipo+"' ");
            if(r.next())
            {
                cons = r.getInt(1);
            }
            aux = cons+1;
            this.insertUpd("update consecutivo set cons = "+aux+" where tipo = '"+tipo+"'");
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return cons;
    }
}
